# Translation of Postfix debconf template to Swedish
# Copyright (C) 2012-2017 Martin Bagge <brother@bsnet.se>
# This file is distributed under the same license as the postfix package.
#
# Martin Ågren <martin.agren@gmail.com>, 2008.
# Martin Bagge <brother@bsnet.se>, 2012, 2013, 2017
msgid ""
msgstr ""
"Project-Id-Version: postfix_2.5.2-2_sv\n"
"Report-Msgid-Bugs-To: postfix@packages.debian.org\n"
"POT-Creation-Date: 2018-12-10 02:32-0500\n"
"PO-Revision-Date: 2017-01-02 10:53+0100\n"
"Last-Translator: Martin Bagge / brother <brother@bsnet.se>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Add a 'mydomain' entry in main.cf for upgrade?"
msgstr "Lägg till en \"mydomain\"-post i main.cf inför uppgraderingen?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"Postfix version 2.3.3-2 and later require changes in main.cf. Specifically, "
"mydomain must be specified, since hostname(1) is not a fully qualified "
"domain name (FQDN)."
msgstr ""
"Postfix version 2.3.3-2 och senare kräver ändringar i main.cf. Specifikt "
"måste \"mydomain\" anges eftersom hostname(1) inte är ett fullständigt "
"kvalificerat domännamn (FQDN)."

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"Failure to fix this will result in a broken mailer. Decline this option to "
"abort the upgrade, giving you the opportunity to add this configuration "
"yourself. Accept this option to automatically set mydomain based on the FQDN "
"of the machine."
msgstr ""
"Om inte du rättar till dessa felaktigheter kommer e-postsystemet inte att "
"fungera korrekt. Vägra denna inställning för att avbryta uppgraderingen och "
"ge dig möjligheten att lägga till denna konfiguration själv.  Acceptera "
"inställningen för att automatiskt ställa in \"mydomain\" baserat på FQDN för "
"maskinen."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Set smtpd_relay_restrictions in main.cf for upgrade?"
msgstr "Ska smtpd_relay_restrictions i main.cf ställas in för uppgradering?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"Postfix version 2.10 adds smtpd_relay_restrictions, to separate relaying "
"restrictions from recipient restrictions, and you have a non-default value "
"for smtpd_recipient_restrictions."
msgstr ""
"Postfix version 2.10 lägger till smtpd_relay_restrictions för att separera "
"restriktioner för vidaresändning och mottagare. Det här systemet har ett "
"ickestandardiserat värde för smtpd_recipient_restrictions."

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"Failure to do this may result in deferred or bounced mail after the "
"upgrade.  Accept this option to set smtpd_relay_restrictions equal to "
"smtpd_recipient_restrictions."
msgstr ""
"Om detta inte justeras kan det innebära att e-post-meddelanden avvisas eller "
"studsar efter uppgraderingen. Aktivera detta alternativ för att ange samma "
"värde för smtpd_relay_restrictions som för smtpd_recipient_restrictions."

#. Type: boolean
#. Description
#: ../templates:3001
#, fuzzy
#| msgid "Update configuration to avoid compatibility warnings"
msgid "Update configuration to avoid compatibility warnings?"
msgstr "Uppdatera inställningar för att undvika kompatibilitetsvarningar"

#. Type: boolean
#. Description
#: ../templates:3001
msgid ""
"This upgrade of postfix changes some default values in the configuration. As "
"part of this upgrade, the following will be changed: (1) chrooted components "
"will be changed from '-' to 'y' in master.cf, and (2) myhostname will be set "
"to a fully-qualified domain name if it is not already such.  The install "
"will be aborted if you do not allow the change."
msgstr ""
"Denna version av postfix ändrar några standardvärden i inställningarna. Som "
"del i denna uppgradering kommer följande att ändras: (1) chrootade "
"komponenter kommer att ändras från \"-\" till \"y\" i master.cf och (2) "
"myhostname kommer att sättas till ett komplett kvalificerat domännamn om det "
"inte redan är ett sådant. Installationen kommer att avbrytas om du inte "
"tillåter ändringarna."

#. Type: boolean
#. Description
#: ../templates:4001
msgid "Update master.cf for merged lmtp/smtp binary?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:4001
#, fuzzy
#| msgid ""
#| "This upgrade of postfix changes where daemons are located, and your "
#| "postfix configuration explicitly specifies the old location.  The install "
#| "will be aborted if you do not allow the change."
msgid ""
"This upgrade of postfix drops the \"lmtp\" symlink, and your configuration "
"(master.cf) refers to it: lmtp was merged into smtp long ago.  The install "
"will be aborted if you do not allow the change."
msgstr ""
"Denna uppgradering av postfix ändrar var tjänster installeras och dina "
"inställningar pekar ut den gamla positionen. Installationen avbryts om du "
"inte tillåter förändringen."

#. Type: boolean
#. Description
#: ../templates:5001
#, fuzzy
#| msgid "Update main.cf for daemon_directory change"
msgid "Update main.cf for daemon_directory change?"
msgstr "Uppdatera main.cf för ändringen av daemon_directory"

#. Type: boolean
#. Description
#: ../templates:5001
msgid ""
"This upgrade of postfix changes where daemons are located, and your postfix "
"configuration explicitly specifies the old location.  The install will be "
"aborted if you do not allow the change."
msgstr ""
"Denna uppgradering av postfix ändrar var tjänster installeras och dina "
"inställningar pekar ut den gamla positionen. Installationen avbryts om du "
"inte tillåter förändringen."

#. Type: boolean
#. Description
#: ../templates:6001
#, fuzzy
#| msgid "Update dynamicmaps.cf for 3.0"
msgid "Update dynamicmaps.cf for 3.0?"
msgstr "Uppdatera dynamicmaps.cf för 3.0"

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"Postfix version 3.0 changes how dynamic maps are delivered, and your "
"dynamicmaps.cf does not reflect that.  Accept this option to convert "
"dynamicmaps.cf to the version required for 3.0."
msgstr ""
"Postfix version 3.0 har ändrat hur dynamisk mappning levereras och din "
"dynamicmaps.cf återspeglar inte detta. Automatisk konvertering av "
"dynamicmaps.cf till version 3.0 kan genomföras."

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Add 'sqlite' entry to dynamicmaps.cf?"
msgstr "Ska \"sqlite\" läggas till i dynamicmaps.cf?"

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"Postfix version 2.9 adds sqlite support to maps, but your dynamicmaps.cf "
"does not reflect that.  Accept this option to add support for sqlite maps."
msgstr ""
"Postfix version 2.0 lägger till stöd för sqlite-mappning men filen "
"dynamicmaps.cf visar inte detta. Godkänn detta alternativ för att lägga till "
"stöd för sqlite-mappning."

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Install postfix despite an unsupported kernel?"
msgstr "Installera postfix även om kärnan inte stöds?"

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Postfix uses features that are not found in kernels prior to 2.6. If you "
"proceed with the installation, Postfix will not run."
msgstr ""
"Postfix använder funktioner som inte finns i kärnor före version 2.6. Om du "
"fortsätter med installationen kommer Postfix inte att kunna starta."

#. Type: boolean
#. Description
#: ../templates:9001
msgid "Correct retry entry in master.cf for upgrade?"
msgstr "Korrigera återförsöksposten i master.cf inför uppgradering?"

#. Type: boolean
#. Description
#: ../templates:9001
msgid ""
"Postfix version 2.4 requires that the retry service be added to master.cf."
msgstr ""
"Postfix version 2.4 och senare kräver att återförsöksposten läggs till i "
"master.cf."

#. Type: boolean
#. Description
#: ../templates:9001
msgid ""
"Failure to fix this will result in a broken mailer. Decline this option to "
"abort the upgrade, giving you the opportunity to add this configuration "
"yourself. Accept this option to automatically make master.cf compatible with "
"Postfix 2.4 in this respect."
msgstr ""
"Om du inte rättar till dessa felaktigheter kommer e-postsystemet inte att "
"fungera korrekt. Vägra denna inställning för att avbryta uppgraderingen och "
"få möjlighet att lägga till denna konfiguration själv.  Acceptera "
"inställningen för att automatiskt göra master.cf kompatibel med Postfix 2.4 "
"i det här avseendet."

#. Type: boolean
#. Description
#: ../templates:10001
msgid "Correct tlsmgr entry in master.cf for upgrade?"
msgstr "Korrigera tlsmgr-posten i master.cf inför uppgradering?"

#. Type: boolean
#. Description
#: ../templates:10001
msgid "Postfix version 2.2 has changed the invocation of tlsmgr."
msgstr "Postfix version 2.2 har ändringar i uppstarten av tlsmgr."

#. Type: boolean
#. Description
#: ../templates:10001
msgid ""
"Failure to fix this will result in a broken mailer. Decline this option to "
"abort the upgrade, giving you the opportunity to add this configuration "
"yourself. Accept this option to automatically make master.cf compatible with "
"Postfix 2.2 in this respect."
msgstr ""
"Om inte du rättar till dessa felaktigheter kommer e-postsystemet inte att "
"fungera korrekt. Vägra denna inställning för att avbryta uppgraderingen och "
"ge dig möjligheten att lägga till denna konfiguration själv.  Acceptera "
"inställningen för att automatiskt göra master.cf kompatibel med Postfix 2.2 "
"i det här avseendet."

#. Type: boolean
#. Description
#: ../templates:11001
msgid "Ignore incorrect hostname entry?"
msgstr "Ignorera felaktig \"hostname\"-post?"

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"The string '${enteredstring}' does not follow RFC 1035 and does not appear "
"to be a valid IP address."
msgstr ""
"Strängen \"${enteredstring}\" följer inte RFC 1035 och verkar inte vara en "
"giltig IP-adress."

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"RFC 1035 states that 'each component must start with an alphanum, end with "
"an alphanum and contain only alphanums and hyphens. Components must be "
"separated by full stops.'"
msgstr ""
"RFC 1035 fastslår att \"varje komponent måste börja med ett alfanumeriskt "
"tal, sluta med ett alfanumeriskt tal och får endast innehålla alfanumeriska "
"tal och bindestreck. Komponenter måste separeras med fullständiga stopp.\""

#. Type: boolean
#. Description
#: ../templates:11001
msgid "Please choose whether you want to keep that choice anyway."
msgstr "Välj huruvida du vill behålla valet ändå."

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../templates:12001
msgid "No configuration"
msgstr "Ingen konfiguration"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../templates:12001
msgid "Internet Site"
msgstr "Internetsystem"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../templates:12001
msgid "Internet with smarthost"
msgstr "Internet med smart värd"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../templates:12001
msgid "Satellite system"
msgstr "Satellitsystem"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../templates:12001
msgid "Local only"
msgstr "Endast lokalt"

#. Type: select
#. Description
#: ../templates:12002
msgid "General type of mail configuration:"
msgstr "Allmän typ av e-postkonfiguration:"

#. Type: select
#. Description
#: ../templates:12002
msgid ""
"Please select the mail server configuration type that best meets your needs."
msgstr ""
"Välj den konfigurationstyp för e-postservern som bäst passar dina behov."

#. Type: select
#. Description
#: ../templates:12002
msgid ""
" No configuration:\n"
"  Should be chosen to leave the current configuration unchanged.\n"
" Internet site:\n"
"  Mail is sent and received directly using SMTP.\n"
" Internet with smarthost:\n"
"  Mail is received directly using SMTP or by running a utility such\n"
"  as fetchmail. Outgoing mail is sent using a smarthost.\n"
" Satellite system:\n"
"  All mail is sent to another machine, called a 'smarthost', for delivery.\n"
" Local only:\n"
"  The only delivered mail is the mail for local users. There is no network."
msgstr ""
" Ingen konfiguration:\n"
"  Bör väljas för att inte röra den befintliga konfiguration.\n"
" Internet-system:\n"
"  Post skickas och tas emot direkt med SMTP.\n"
" Internet med smart värd:\n"
"  Post tas emot direkt med SMTP eller genom att köra verktyg som\n"
"  fetchmail. Utgående post skickas via en smart värd.\n"
" Satellitsystem:\n"
"  All post skickas till en annan maskin, en så kallad \"smarthost\", för "
"leverans.\n"
" Endast lokalt:\n"
"  Den enda posten som levereras är posten för lokala användare. Inget "
"nätverk."

#. Type: error
#. Description
#: ../templates:13001
msgid "Postfix not configured"
msgstr "Postfix är inte konfigurerad"

#. Type: error
#. Description
#: ../templates:13001
msgid ""
"You have chosen 'No Configuration'. Postfix will not be configured and will "
"not be started by default. Please run 'dpkg-reconfigure postfix' at a later "
"date, or configure it yourself by:\n"
" - Editing /etc/postfix/main.cf to your liking;\n"
" - Running 'service postfix start'."
msgstr ""
"Du har valt \"Ingen konfiguration\". Postfix kommer inte att konfigureras "
"och kommer som standard inte att starta upp. Kör \"dpkg-reconfigure postfix"
"\" vid ett senare tillfälle eller konfigurera det själv genom att:\n"
" - Redigera /etc/postfix/main.cf för att passa dina behov;\n"
" - Köra \"service postfix start\"."

#. Type: string
#. Description
#: ../templates:14001
msgid "System mail name:"
msgstr "Systemets e-postnamn:"

#. Type: string
#. Description
#: ../templates:14001
msgid ""
"The \"mail name\" is the domain name used to \"qualify\" _ALL_ mail "
"addresses without a domain name. This includes mail to and from <root>: "
"please do not make your machine send out mail from root@example.org unless "
"root@example.org has told you to."
msgstr ""
"Parametern \"mail name\" är domännamnet som används för att \"kvalificera\" "
"_ALLA_ e-postadresser utan ett domännamn. Detta inkluderar post till och "
"från <root>: låt inte din maskin skicka ut post från root@example.org såvida "
"inte root@example.org har bett dig göra det."

#. Type: string
#. Description
#: ../templates:14001
msgid ""
"This name will also be used by other programs. It should be the single, "
"fully qualified domain name (FQDN)."
msgstr ""
"Detta namn kommer även att användas av andra program. Det bör vara ett enda, "
"fullständigt kvalificerat domännamn (FQDN)."

#. Type: string
#. Description
#. Translators, please do NOT translate 'example.org' whch is registered
#. as a domain name reserved for documentation as per RFC 2606
#: ../templates:14001
msgid ""
"Thus, if a mail address on the local host is foo@example.org, the correct "
"value for this option would be example.org."
msgstr ""
"Följaktligen, om en e-postadress på den lokala maskinen är foo@example.org, "
"skulle det korrekta värde för den här inställning vara example.org."

#. Type: string
#. Description
#: ../templates:15001
msgid "Other destinations to accept mail for (blank for none):"
msgstr "Andra destinationer att ta emot e-post för? (lämna blank om ingen):"

#. Type: string
#. Description
#: ../templates:15001
msgid ""
"Please give a comma-separated list of domains for which this machine should "
"consider itself the final destination. If this is a mail domain gateway, you "
"probably want to include the top-level domain."
msgstr ""
"Ange en kommaseparerad lista över domäner som denna maskin ska anse sig "
"själv som den slutgiltiga destinationen för. Om detta är en gateway för e-"
"postdomäner vill du antagligen inkludera toppnivådomänen."

#. Type: string
#. Description
#: ../templates:16001
msgid "SMTP relay host (blank for none):"
msgstr "SMTP-värd för vidaresändning (lämna blank för ingen alls):"

#. Type: string
#. Description
#: ../templates:16001
msgid ""
"Please specify a domain, host, host:port, [address] or [address]:port. Use "
"the form [destination] to turn off MX lookups. Leave this blank for no relay "
"host."
msgstr ""
"Ange en domän, värd, värd:port,  [adress] eller [adress]:port. Använd "
"formatet [destination] för att stänga av MX-uppslag. Lämna blank för att "
"inte använda en vidaresändningsvärd."

#. Type: string
#. Description
#: ../templates:16001
msgid "Do not specify more than one host."
msgstr "Ange inte fler än en värd."

#. Type: string
#. Description
#: ../templates:16001
msgid ""
"The relayhost parameter specifies the default host to send mail to when no "
"entry is matched in the optional transport(5) table. When no relay host is "
"given, mail is routed directly to the destination."
msgstr ""
"Parametern \"relayhost\" (relävärd) anger den standardvärd som post ska "
"skickas till när ingen post matchas i den valfria transport(5)-tabellen. När "
"ingen relävärd angivits kommer post att skickas direkt till destinationen."

#. Type: boolean
#. Description
#: ../templates:17001
msgid "Use procmail for local delivery?"
msgstr "Använda procmail för lokala leveranser?"

#. Type: boolean
#. Description
#: ../templates:17001
msgid "Please choose whether you want to use procmail to deliver local mail."
msgstr "Välj huruvida du vill använda procmail för att leverera lokal post."

#. Type: boolean
#. Description
#: ../templates:17001
msgid ""
"Note that if you use procmail to deliver mail system-wide, you should set up "
"an alias that forwards mail for root to a real user."
msgstr ""
"Observera att om du använder procmail för att leverera post över hela "
"systemet bör du ställa in ett alias som skickar vidare post för root till en "
"vanlig användare."

#. Type: select
#. Choices
#: ../templates:18001
msgid "all"
msgstr "alla"

#. Type: select
#. Choices
#: ../templates:18001
msgid "ipv6"
msgstr "ipv6"

#. Type: select
#. Choices
#: ../templates:18001
msgid "ipv4"
msgstr "ipv4"

#. Type: select
#. Description
#: ../templates:18002
msgid "Internet protocols to use:"
msgstr "Internetprotokoll att använda:"

#. Type: select
#. Description
#: ../templates:18002
msgid ""
"By default, whichever Internet protocols are enabled on the system at "
"installation time will be used. You may override this default with any of "
"the following:"
msgstr ""
"Som standard kommer de internetprotokoll som är aktiverade på systemet att "
"användas. Du kan åsidosätta den här inställningen genom att välja någon av "
"följande:"

#. Type: select
#. Description
#: ../templates:18002
msgid ""
" all : use both IPv4 and IPv6 addresses;\n"
" ipv6: listen only on IPv6 addresses;\n"
" ipv4: listen only on IPv4 addresses."
msgstr ""
" alla: använd både IPv4 och IPv6-adresser;\n"
" ipv6: lyssna endast på IPv6-adresser;\n"
" ipv4: lyssna endast på IPv4-adresser."

#. Type: string
#. Description
#: ../templates:19001
msgid "Local address extension character:"
msgstr "Tecken för lokala adressutökningar:"

#. Type: string
#. Description
#: ../templates:19001
msgid ""
"Please choose the character that will be used to define a local address "
"extension."
msgstr ""
"Välj det tecken som ska användas för att definiera en lokal adressutökning."

#. Type: string
#. Description
#: ../templates:19001
msgid "To not use address extensions, leave the string blank."
msgstr "För att inte använda adressutökningar, lämna fältet blankt."

#. Type: error
#. Description
#: ../templates:20001
msgid "Bad recipient delimiter"
msgstr "Felaktig avgränsning för mottagare"

#. Type: error
#. Description
#: ../templates:20001
msgid ""
"The recipient delimiter must be a single character. '${enteredstring}' is "
"what you entered."
msgstr ""
"Avgränsaren för mottagaren måste vara ett enda tecken. Du angav "
"\"${enteredstring}\"."

#. Type: boolean
#. Description
#: ../templates:21001
msgid "Force synchronous updates on mail queue?"
msgstr "Tvinga synkroniserade uppdateringar av postkön?"

#. Type: boolean
#. Description
#: ../templates:21001
msgid ""
"If synchronous updates are forced, then mail is processed more slowly. If "
"not forced, then there is a remote chance of losing some mail if the system "
"crashes at an inopportune time, and you are not using a journaled filesystem "
"(such as ext3)."
msgstr ""
"Om synkroniserade uppdateringar tvingas igenom kommer posten att behandlas "
"långsammare. Om den inte tvingas finns en liten chans att viss post går "
"förlorad om systemet kraschar vid fel tidpunkt och du inte använder ett "
"journalskrivande filsystem (exempelvis ext3)."

#. Type: string
#. Description
#: ../templates:22001
msgid "Local networks:"
msgstr "Lokala nätverk:"

#. Type: string
#. Description
#: ../templates:22001
msgid ""
"Please specify the network blocks for which this host should relay mail. The "
"default is just the local host, which is needed by some mail user agents. "
"The default includes local host for both IPv4 and IPv6. If just connecting "
"via one IP version, the unused value(s) may be removed."
msgstr ""
"Ange de nätverksblock som denna maskin ska vidarebefordra e-post för. "
"Standard är att endast vidarebefordra för den lokala maskinen, vilket krävs "
"för vissa e-postagenter. Standard inkluderar den lokala maskinen både för "
"IPv4 och IPv6. Om du bara ansluter via en version av IP, kan ett eller flera "
"oanvända värden tas bort."

#. Type: string
#. Description
#: ../templates:22001
msgid ""
"If this host is a smarthost for a block of machines, you need to specify the "
"netblocks here, or mail will be rejected rather than relayed."
msgstr ""
"Om den här värden är en smart värd för ett nätverk av maskiner behöver du "
"ange de nätblocken här, eller så kommer post att vägras ta emot istället för "
"att vidarebefordras."

#. Type: string
#. Description
#: ../templates:22001
msgid ""
"To use the postfix default (which is based on the connected subnets), leave "
"this blank."
msgstr ""
"Lämna denna blank för att använda standardvärdet för postfix (som är baserat "
"på anslutna nätverk)."

#. Type: string
#. Description
#: ../templates:23001
msgid "Mailbox size limit (bytes):"
msgstr "Storleksgräns för postlåda (i byte):"

#. Type: string
#. Description
#: ../templates:23001
msgid ""
"Please specify the limit that Postfix should place on mailbox files to "
"prevent runaway software errors. A value of zero (0) means no limit. The "
"upstream default is 51200000."
msgstr ""
"Vilken gräns ska Postfix sätta på postlådefiler för att förhindra att "
"programvarufel skriver för mycket data. Ett värde på noll (0) betyder att "
"det inte finns någon gräns. Standard från utvecklarna är 51200000."

#. Type: string
#. Description
#: ../templates:24001
msgid "Root and postmaster mail recipient:"
msgstr "E-postmottagare för root och postmaster:"

#. Type: string
#. Description
#: ../templates:24001
msgid ""
"Mail for the 'postmaster', 'root', and other system accounts needs to be "
"redirected to the user account of the actual system administrator."
msgstr ""
"E-post till \"postmaster\", \"root\" och andra systemkonton behöver "
"omdirigeras till användarkontot för den riktiga systemadministratören."

#. Type: string
#. Description
#: ../templates:24001
msgid ""
"If this value is left empty, such mail will be saved in /var/mail/nobody, "
"which is not recommended."
msgstr ""
"Om det här värdet utelämnas kommer sådan e-post att sparas i /var/mail/"
"nobody, vilket inte rekommenderas."

#. Type: string
#. Description
#: ../templates:24001
msgid "Mail is not delivered to external delivery agents as root."
msgstr ""
"E-post kommer inte att levereras till externa leveransagenter som root."

#. Type: string
#. Description
#: ../templates:24001
msgid ""
"If you already have a /etc/aliases file and it does not have an entry for "
"root, then you should add this entry.  Leave this blank to not add one."
msgstr ""
"Om du redan har filen /etc/aliases och den saknar instruktioner för root ska "
"detta läggas till. Lämna den blank för att inte lägga till någon instruktion."

#. Type: boolean
#. Description
#: ../templates:25001
msgid "Run newaliases command?"
msgstr ""
